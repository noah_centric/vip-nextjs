const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const withCSS = require('@zeit/next-css')
//const MiniCssExtractPlugin = require('mini-css-extract-plugin')
module.exports = {
	webpack: (config, options) => {
		if (config.resolve.plugins) {
			config.resolve.plugins.push(new TsconfigPathsPlugin());
		} else {
			config.resolve.plugins = [new TsconfigPathsPlugin()];
		}

		config.module.rules.push({
			                         test: /\.css$/,
			                         use:  [
				                         // {
					                     //     loader:  MiniCssExtractPlugin.loader,
					                     //     options: {
						                 //         publicPath: './static'
					                     //     }
				                         // },
				                         {
					                         loader:  'css-loader',
					                         options: {
						                         modules:   true,
						                         sourceMap: true
					                         }
				                         }
			                         ]
		                         });

		config.module.rules.push({
			                         test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
			                         use:  {
				                         loader:  'url-loader',
				                         options: {
					                         limit: 100000,
					                         name:  '[name].[ext]'
				                         }
			                         }
		                         })

		return config;
	},
	target:  "serverless"
}