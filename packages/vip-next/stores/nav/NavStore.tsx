import { action, computed, observable, qs, lockr } from '@api'
import {stores} from '@stores';
import {runInAction} from 'mobx';
import Router from 'next/router'

type LibraryTypeName = string
export type AvailableQueryParams = {
  /**
   * For GridList / ProductDashboard
   */
  sortBy_name?: string | undefined
  sortBy_order?: string | undefined
  cardView?: string[] | string | undefined
  imageSpec?: string | undefined
  // The actual hierarchy separated by commas
  groupBy?: string  | undefined
  groupBy_expanded?: string[] | string | undefined
  groupBy_nowrap?: null | undefined
  groupBy_compact?: null | undefined
  groupBy_expandedFirst?: null | undefined
  groupBy_style?: 'flat' | 'card' | undefined
  groupBy_rowsPerPage?: string | undefined
  groupBy_page?: string | undefined
  groupBy_view?: 'card' | 'table'
  orderBy_spec?: string | undefined
  orderBy_specField?: string | undefined
  orderBy_productField?: string | undefined
  orderBy_attributeField?: string | undefined
  filter?: string | undefined
  zoom?: string | undefined
  search?: string | undefined
  qr?: null | undefined

  /**
   * For product sidebar overlay
   */
  product?: string
  note?: null | undefined | string,
  noteContainer?: null | undefined | string,
  notes?: null | undefined | string,
  noteAction?: undefined | 'add' | 'edit',
  specificationType?: string | undefined,
  specification?: string | undefined,
  
  /**
   * For specification sidebar in dashboard
   */
  sidebar?: null | undefined
  sidebarTab?: string | undefined
  list?: string | undefined,
  listView?: string | undefined,
  libraryId?: string | undefined,
  attributeDefinition?: string | undefined,
  specZoom?: string | undefined;
  sidebarFilter?: 'plm' | string | undefined

  /**
   * For manage specification library screens
   */
  view?: undefined | string
  libraryTab?: 'plm-library' | 'seasonal-library' | 'site-library' | string | undefined,
  library?: string | undefined
  id?: string | undefined

  rowsPerPage?: undefined | string
  page?: undefined | string

  /**
   * For manage attribute screens
   */
  tags?: string | undefined,

  /**
   * Mobx Debugging
   */
  mobx?: null | undefined
}

export class NavStore {
  constructor() {
  }

  // Only will have matches of whatever was handed to updateLocation()
 // @observable match: match<{season, category1, category2}>

  urlString = ({pathname, search}) => {
    return `${pathname}?${search}`
  }

  @action routeToLoginPage = () => {
    if (TEST) {
      console.warn('System requested navigate to /login')
      return
    }

    Router.push('/login')
  }

  @action routeToLogoutPage = () => {
    if (TEST) {
      console.warn('System requested navigate to /logout')
      return
    }

    if (Router.pathname != '/login') {
      Router.push('/logout')
    }
  }

  @action siteHeader_goBack = () => {
    if (TEST) {
      console.warn('System requested navigate to previous page')
      return
    }

    //this.siteHeader_navigateToLibrary(SeasonalLibraryMapping[this.queryParams.library])
  }

  @action siteHeader_navigateToLibrary = (library) => {
    // if (vlp.isVLPView) {
    //   nav.routeTo({library, queryParams: vlp.queryParams})
    // } else {
    //  nav.routeTo({library})
    // }
  }

  // urlForObject = (o: any) => {
  //   const typename            = o.__typename || (o.type && o.type.name)
  //   const {urlFor, urlString} = this
  //   switch (typename) {
  //     case 'Product': {
  //       return `${window.origin}${urlString(urlFor({library: 'Products', queryParams: {product: o.id}}))}`
  //     }
  //     case 'Color': {
  //       return `${window.origin}${urlString(urlFor({library: 'SeasonalColors', queryParams: {library: 'colors', id: o.id}}))}`
  //     }
  //     case 'Material': {
  //       return `${window.origin}${urlString(urlFor({library: 'SeasonalMaterials', queryParams: {library: 'materials', id: o.id}}))}`
  //     }
  //     case 'Sketch': {
  //       return `${window.origin}${urlString(urlFor({library: 'SeasonalStyles', queryParams: {library: 'styles', id: o.id}}))}`
  //     }
  //     case 'Hardware': {
  //       return `${window.origin}${urlString(urlFor({library: 'SeasonalHardware', queryParams: {library: 'hardware', id: o.id}}))}`
  //     }
  //
  //     default: {
  //       false && console.warn('Unable to generate URL for object - determine type',{o, typename})
  //       return ''
  //     }
  //   }
  // }
}

export const nav = new NavStore()