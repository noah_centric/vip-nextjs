// This class works as an interceptor to trim the each http request variable to remove empty whitespace


import {ApolloLink} from 'apollo-link';

class UtilityInterceptor {
    trimMiddleware;

    constructor() {
        this.trimMiddleware = new ApolloLink((operation, forward) => {
            operation.variables = JSON.parse(JSON.stringify(operation.variables).replace(/"\s+|\s+"/g, '"'));
            return forward(operation)
          });
    }
}

export const utilityInterceptor = new UtilityInterceptor();