import React, {useMemo} from 'react'
import Head from 'next/head'
import {ApolloProvider} from '@apollo/react-hooks'
import {ApolloClient} from 'apollo-client'
import {InMemoryCache} from 'apollo-cache-inmemory'
import {HttpLink} from 'apollo-link-http'
import fetch from 'isomorphic-unfetch'
export let client : ApolloClient<{}>

/**
 * Creates and provides the apolloContext
 * to a next.js PageTree. Use it by wrapping
 * your PageComponent via HOC pattern.
 * @param {Function|Class} PageComponent
 * @param {Object} [config]
 * @param {Boolean} [config.ssr=true]
 */
export function withApollo(PageComponent, {ssr = true} = {}) {
  const WithApollo = ({apolloClient, apolloState, ...pageProps}) => {
    const client = useMemo(
      () => apolloClient || initApolloClient(apolloState),
      []
    )
    return (
      <ApolloProvider client={client}>
        <PageComponent {...pageProps} />
      </ApolloProvider>
    )
  }

  // Set the correct displayName in development
  if (process.env.NODE_ENV !== 'production') {
    const displayName =
            PageComponent.displayName || PageComponent.name || 'Component'

    if (displayName === 'App') {
      console.warn('This withApollo HOC only works with PageComponents.')
    }

    WithApollo.displayName = `withApollo(${displayName})`
  }

  if (ssr || PageComponent.getInitialProps) {
    WithApollo.getInitialProps = async ctx => {
      const {AppTree} = ctx

      // Initialize ApolloClient, add it to the ctx object so
      // we can use it in `PageComponent.getInitialProp`.
      const apolloClient = (ctx.apolloClient = initApolloClient())

      // Run wrapped getInitialProps methods
      let pageProps = {}
      if (PageComponent.getInitialProps) {
        pageProps = await PageComponent.getInitialProps(ctx)
      }

      // Only on the server:
      if (typeof window === 'undefined') {
        // When redirecting, the response is finished.
        // No point in continuing to render
        if (ctx.res && ctx.res.finished) {
          return pageProps
        }

        // Only if ssr is enabled
        if (ssr) {
          try {
            // Run all GraphQL queries
            const {getDataFromTree} = await import('@apollo/react-ssr')
            await getDataFromTree(
              <AppTree
                pageProps={{
                  ...pageProps,
                  apolloClient
                }}
              />
            )
          } catch (error) {
            // Prevent Apollo Client GraphQL errors from crashing SSR.
            // Handle them in components via the data.error prop:
            // https://www.apollographql.com/docs/react/api/react-apollo.html#graphql-query-data-error
            console.error('Error while running `getDataFromTree`', error)
          }

          // getDataFromTree does not call componentWillUnmount
          // head side effect therefore need to be cleared manually
          Head.rewind()
        }
      }

      // Extract query data from the Apollo store
      const apolloState = apolloClient.cache.extract()

      return {
        ...pageProps,
        apolloState
      }
    }
  }

  return WithApollo
}

/**
 * Always creates a new apollo client on the server
 * Creates or reuses apollo client in the browser.
 * @param  {Object} initialState
 */
function initApolloClient(initialState?) {
  // Make sure to create a new client for every server-side request so that data
  // isn't shared between connections (which would be bad)
  if (typeof window === 'undefined') {
    return createApolloClient(initialState)
  }

  // Reuse client on the client-side
  if (!client) {
    client = createApolloClient(initialState)
  }

  return client
}

import {BatchHttpLink} from 'apollo-link-batch-http'

// import {ApolloClient, BatchHttpLink, InMemoryCache, withClientState, createNetworkStatusNotifier, ApolloLink, WebSocketLink, getMainDefinition, split, HttpLink} from '@apollo'
// import {lockr, _, config} from '@api'
// import {localCache} from './defaults'
// import {setContext} from 'apollo-link-context';
import config from '../../config/index'
import {split, ApolloLink} from 'apollo-link';
import {authLink} from './authLink';

//import {createNetworkStatusNotifier} from 'react-apollo-network-status'
/**
 * Creates and configures the ApolloClient
 * @param  {Object} [initialState={}]
 */
function createApolloClient(initialState = {}) {
  const batchLinkWithAuthToken = new BatchHttpLink({
    fetch,
    uri: config.apiGatewayEndpoint, batchMax: 10, batchInterval: 50, batchKey: (operation): string => {
      return operation && operation.getContext() && operation.getContext().batchKey ? operation.getContext().batchKey : 'default'
    }
  })
  const httpLinkWithAuthToken  = new HttpLink({})

  const linkToUse = split(
    operation => {
      // console.log(operation)
      const context = operation.getContext()
      // Return true to NOT be batched
      return !context.batch || operation.query.definitions[0]['operation'] == 'mutation'
    },
    httpLinkWithAuthToken,
    batchLinkWithAuthToken
  )

  // Check out https://github.com/zeit/next.js/pull/4611 if you want to use the AWSAppSyncClient
  return new ApolloClient({
    ssrMode: typeof window === 'undefined', // Disables forceFetch on the server (so queries are only run once)
    link: ApolloLink.from([authLink, linkToUse]),
    cache:   new InMemoryCache().restore(initialState)
  })
}
//
// // Set up apollo-link-state
// const cache     = new InMemoryCache()
// const stateLink = withClientState({
//   resolvers: {},
//   defaults:  {...localCache.typeDefault},
//   cache
// })
//
// const batchLinkWithAuthToken = new BatchHttpLink({
//   uri: config.apiGatewayEndpoint, batchMax: 10, batchInterval: 50, batchKey: (operation): string => {
//     return operation && operation.getContext() && operation.getContext().batchKey ? operation.getContext().batchKey : 'default'
//   }
// })
// const httpLinkWithAuthToken  = new HttpLink({uri: config.apiGatewayEndpoint})
//
// let graphQLSubscriptionUrl = config.wsApiGatewayEndpoint
// if (!_.includes(graphQLSubscriptionUrl, 'ws://')) {
//   graphQLSubscriptionUrl = ((window.location.protocol === 'https:') ? 'wss://' : 'ws://') + window.location.host + graphQLSubscriptionUrl
// }
// const wsLink = new WebSocketLink({
//   uri:     graphQLSubscriptionUrl,
//   options: {
//     lazy:             true,
//     reconnect:        true,
//     connectionParams: () => {
//       return {Authorization: lockr.get('access_token') ? lockr.get('access_token') : null}
//     }
//   }
// })
//
// const debugHeaderLink = setContext((op, prev) => {
//   if (!DEBUG) return prev
//
//   const {headers, alias} = prev
//
//   if (alias) {
//     return {
//       ...prev,
//       headers: {
//         ...headers,
//         'x-debug-alias': alias
//       }
//     }
//   }
//   return prev
// })
//
// const linkToUse = split(
//   operation => {
//     // console.log(operation)
//     const context = operation.getContext()
//     // Return true to NOT be batched
//     return !context.batch || operation.query.definitions[0].operation == 'mutation'
//   },
//   httpLinkWithAuthToken,
//   batchLinkWithAuthToken
// )
//
// const httpLink = ApolloLink.from([networkStatusNotifierLink, utilityInterceptor.trimMiddleware, authLink, /* logUtils.loggingMiddleware, logUtils.loggingAfterware, */, debugHeaderLink, linkToUse]);
// const link     = split(
//   ({query}) => {
//     const {kind, operation} = getMainDefinition(query as any) as any
//     return kind === 'OperationDefinition' && operation === 'subscription'
//   },
//   wsLink,
//   httpLink
// )
// this.client    = new ApolloClient({
//   link: ApolloLink.from([stateLink, link]),
//   cache
// })
