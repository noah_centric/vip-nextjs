/* global fetch */
import { stores } from '@stores';
import { lockr, jwt } from '@api'
import {observable} from 'mobx';

lockr.prefix = 'design-board-'

/**
 * Stores access token and it's expiry value in local storage
 *
 * @param {string} token
 */
const saveAccessToken = (token) => {
  const decodedAccessTokenValue = jwt.decode(token)
  lockr.set('access_token_expiry_date', decodedAccessTokenValue.exp)
  lockr.set('access_token', token)
}

/**
 * Stores refresh token and it's expiry in local storage
 */
const saveRefreshToken = (token) => {
  const decodedRefeshTokenValue = jwt.decode(token)
  lockr.set('refresh_token_expiry_date', decodedRefeshTokenValue.exp)
  lockr.set('refresh_token', token)
}

/**
 * Stores User object
 *
 * @param {object} user
 */
const saveUser = (accessToken) => {
  // Decode token
  let user = parseToken(accessToken)
  authStore.userIsAuthenticated = true
  lockr.set('user', user)
}

const parseToken = (accessToken: string) => {
  let decodedToken = {}
  try {
    decodedToken = JSON.parse(atob(accessToken.split('.')[1]))
  } catch (e) {
    console.log('Error in parsing token!')
  }
  return decodedToken
}

/**
 * Removes all information related to user (access tokens, refresh token)
 */
const invalidateUser = () => {
  authStore.userIsAuthenticated = false
  lockr.rm('access_token')
  lockr.rm('access_token_expiry_date')
  lockr.rm('refresh_token')
  lockr.rm('refresh_token_expiry_date')
  lockr.rm('user')
  lockr.flush()
}

/**
 * Checks if user is authenticated
 */
const isAuthenticated = () => {
  const r = lockr.get('access_token') != null
  if (authStore && r != authStore.userIsAuthenticated) {
    authStore.userIsAuthenticated = r
  }
  return r
}

const logout = async () => {
  //todo nvs move to this a global site store
  authStore.userIsAuthenticated = false
  let windowZoom = lockr.get('windowZoom')
  lockr.rm('access_token')
  lockr.rm('access_token_expiry_date')
  lockr.rm('refresh_token')
  lockr.rm('refresh_token_expiry_date')
  lockr.rm('user')
  lockr.flush()
  // Do not remove windowZoom setting on log out
  lockr.set('windowZoom', windowZoom)
  const {apollo: {client}} = stores
  try {
    client.clearStore().then(() => {
      client.queryManager.clearStore()
      client.cache.reset()
      client.store.reset()
      //site.reset()
    });
  } catch (err) {
    console.error(err)
  }
}

export class AuthStore {
  @observable userIsAuthenticated = false
}

const authStore = new AuthStore()

export {saveAccessToken, saveRefreshToken, invalidateUser, isAuthenticated, saveUser, logout, authStore}
