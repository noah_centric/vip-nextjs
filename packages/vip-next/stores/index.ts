export * from './apollo'
// export * from './site'
// export * from './user'
export * from './nav'
// export * from './configuration'
// export * from './upload'
// export * from './dev'

import {authStore, AuthStore} from './auth';
//import {UploadStore} from '@stores/upload';
//import {site, SiteStore} from './site'
import {client} from './apollo'
import {nav, NavStore} from './nav'
// import {
//   CatalogStore, DashboardStore, ProductStore, LibraryStore, AttributeStore, SpecificationDashboardStore, TagsStore, PdfExportStore,
//   DashboardDragDropStore, GroupByStore, MassProductCreateStore, SelectionStore, pdfStyles,
//   PdfExportStoreQueryParams, DefaultPdfExportConfig, SpecificationFilterStore, pdf, SidebarStore, sidebar
// } from '@design-board/stores'
// import { configStore as config, ConfigurationStore } from './configuration'
// import { vlp, VLPStore } from './vlp'
// import {strings_en as strings} from './StringsStore'
// import {auth, log, error} from '@utils'
import {ApolloClient, observable} from '@api'

export interface Stores {
  apollo: {client: ApolloClient<any>}
  auth: AuthStore
  // product: ProductStore
  // sidebar: SidebarStore
  // library: LibraryStore
  // attribute: AttributeStore
  // filters: SpecificationFilterStore
  // specDashboard: SpecificationDashboardStore
  nav: NavStore
  // site: SiteStore
  // strings: any,
  // catalog?: CatalogStore,
  // dashboard?: DashboardStore
  // config: ConfigurationStore
  // tags: TagsStore
  // upload: UploadStore
  // vlp: VLPStore
  // pdf: PdfExportStore
}


/**
 * We should ideally have instances of all stores created in one file and
 * then import them anywhere in application so that instance is singleton
 **/
export const stores: Stores = observable({
  //auth, log, error,
  apollo: {client},
  auth: authStore,
  // product:       new ProductStore(),
  // sidebar:       sidebar,
  // library:       new LibraryStore(),
  // attribute:     new AttributeStore(),
  // filters:       new SpecificationFilterStore(),
  // specDashboard: new SpecificationDashboardStore(),
  nav,
  // site,
  // config,
  // strings,
  // tags:          new TagsStore(),
  // upload:        new UploadStore(),
  // pdf:           pdf,
  // //dev: DEBUG && new DevStore()
  // vlp
}, {}, {deep: false})

if (typeof window !== 'undefined') {
  window.centric = stores
}

export {authStore}
/*

export {
  CatalogStore, DashboardStore, ProductStore, LibraryStore, AttributeStore, SpecificationDashboardStore, TagsStore, PdfExportStore, SpecificationFilterStore, pdf, SidebarStore, PdfExportStoreQueryParams, DefaultPdfExportConfig,
  DashboardDragDropStore, sidebar,
  GroupByStore,
  MassProductCreateStore,
  SelectionStore,
  pdfStyles,
  vlp, VLPStore, strings
}
*/
