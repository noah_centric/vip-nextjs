declare module '*.css'
declare module '*.graphql'
declare module '*.csv'
declare module '*.png'
declare module '*.svg'

declare const PRODUCT_NAME: string;
declare const VERSION: string;
declare const DEBUG: boolean;
declare const LOG_EXCEPTION: boolean;
declare const TEST: boolean

declare namespace _ {
}

interface NodeModule {
	exports: any;
	require: NodeRequireFunction;
	id: string;
	filename: string;
	loaded: boolean;
	parent: NodeModule | null;
	children: NodeModule[];
}

declare var module: NodeModule;

declare interface Window {
	FileReader // shouldn't this instead be a raw new FileReader() from webworkers lib?
	FormData
	CustomEvent,
	centric: {
		site?:any,
		nav?: any
	} & any
	ResizeObserver
}

declare interface HTMLElement {
	scrollIntoViewIfNeeded: (b?: boolean) => void
}

declare module '@jaames/iro' {

	export class ColorPicker {
		color?: any;
		constructor(colorPicker: React.ReactInstance, param2: { width: number; height: number; color: string; sliderMargin: number; css: { '.sample': { 'background-color': string } } }) {

		}

		on(colorChange: string, param2: (color, changes) => void) {

		}
	}
}
