import { ApolloProvider, QueryResult }                 from 'react-apollo';
export {ApolloClient} from 'apollo-client'
export {BatchHttpLink} from 'apollo-link-batch-http'
export {HttpLink} from 'apollo-link-http'
export {InMemoryCache} from 'apollo-cache-inmemory'
export {ApolloLink, split} from 'apollo-link'
export {getMainDefinition} from 'apollo-utilities'
export * from 'react-apollo'

export function compose(...funcs) {
  if (funcs.length === 0) {
    return arg => arg
  }

  if (funcs.length === 1) {
    return funcs[0]
  }

  return funcs.reduce((a, b) => (...args) => a(b(...args)))
}

export type InjectedQueryResult<T> = QueryResult<T> & { seasons: Array<T> };
