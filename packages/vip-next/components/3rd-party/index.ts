import * as apollo from './apollo';
import _ from 'lodash'
import * as classNames from 'classnames'
//import InfiniteScroll from 'react-infinite-scroller'
import * as lockr from 'lockr'
export * from './mobx'
import React, {memo} from 'react'
import * as ReactDOM from 'react-dom'
import * as jwt from 'jsonwebtoken'
import * as qs from 'query-string'
import moment from 'moment'
//import SwipeableViews from 'react-swipeable-views'
//import MediaQuery from 'react-responsive'
//import tinycolor from 'tinycolor2'
//import TimeAgo from 'react-timeago'
export * from './mui'

import * as ReactDOMServer from 'react-dom/server'
const {findDOMNode} = ReactDOM
export {ReactDOMServer}
//export {ToastContainer, toast} from 'react-toastify'
import IdleTimer from 'react-idle-timer'
//import SVG from 'react-inlinesvg';
//import Helmet from 'react-helmet';
//import memoize from 'memoize-one'
//import InfiniteScroll from 'react-infinite-scroller'
//import AutoSizer from "react-virtualized-auto-sizer"

//import QRCode from 'qrcode.react'
//import AwesomeQRFn from './awesome-qr/awesome-qr'

// const AwesomeQR: {
//   create: (args: {
//     text?: string,
//     size: number,
//     colorDark?: string,
//     colorLight?: string,
//     margin?: number,
//     backgroundImage?: any,
//     logoImage?: any,
//     binarize?: boolean,
//     typeNumber?: number,
//     /**
//      * 'rgba(0,0,0,0)'
//      */
//     backgroundDimming?: string,
//     bindElement?: any,
//     callback: (dataURI) => void,
//     autoColor?: boolean,
//     maskedDots?: boolean,
//     dotScale?: any,
//     whiteMargin?: number
//   }) => void,
//
// } = new AwesomeQRFn() as any

// import Highlighter from 'react-highlight-words'
// import VisibilitySensor from 'react-visibility-sensor'

export {qs, apollo, _, classNames, lockr, jwt, moment, IdleTimer};
export {React, ReactDOM, findDOMNode}
export {withApollo, Query, Mutation, Subscription, ApolloProvider, ApolloLink, ApolloClient, graphql, compose, ApolloConsumer, useQuery, useMutation, useApolloClient, useSubscription} from './apollo'

import styled, {css} from './styled'

export {styled, css}

export * from './mobx'
//export * from './react-window'

import '@blueprintjs/core/lib/css/blueprint.css'
import * as bp from '@blueprintjs/core'

export {bp}

import {Tooltip} from '@material-ui/core'

Tooltip.defaultProps = {enterDelay: 400}

import {Popover} from '@blueprintjs/core'

Popover.defaultProps.hoverOpenDelay = Popover.defaultProps.hoverCloseDelay = 400
Popover.defaultProps.minimal = true

export * from './wijmo'

export interface StyledProps {
  classes?: any;
  className?: string
  style?: React.CSSProperties;
}

export {withStyles} from '@material-ui/core'

//export * from '@modules/components'

