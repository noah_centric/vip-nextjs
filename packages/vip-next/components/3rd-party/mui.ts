import * as mui from '@material-ui/core'

import {Tooltip} from '@material-ui/core'
import {TabProps} from '@material-ui/core/Tab';
import {LocationDescriptor} from 'history';
import * as React from 'react';

const Tab = mui.Tab as React.ComponentType<TabProps & { to?: LocationDescriptor }>

export {Tab, mui}

Tooltip.defaultProps = Object.assign({}, Tooltip.defaultProps, {disableTriggerFocus: true})