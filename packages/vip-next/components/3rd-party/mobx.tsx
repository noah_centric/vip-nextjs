export * from 'mobx'
export * from 'mobx-react'

import {useObserver} from 'mobx-react'
import {autorun, runInAction, toJS} from 'mobx';
import * as React from 'react'
import styled from 'styled-components';

interface MyProps {
  children: () => void;
  name?: string;
}

export class Autorun extends React.Component<MyProps, {}> {
  _dispose: Function[] = [];

  componentWillMount() {
    const {name} = this.props;  // non-reactive

    this._dispose.push(
      autorun(() => {
        var f = this.props.children as Function;
        f && f();
      }, {name})
    )
  }

  componentWillUnmount() {
    this._dispose.forEach(f => f());
  }

  render() {
    return null;
  }
}

//window['toJS'] = toJS

/**
 * Wraps an inner call to styled() with a useObserver to force observing of changes
 * @param f
 */
export function styledObserver<P, T extends React.ReactElement<P>>(f: (props: P) => T) {
  return styled(props => useObserver(() => f(props)))
}