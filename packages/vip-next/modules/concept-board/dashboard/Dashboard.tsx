import {nav,stores} from '@stores';
import {observable} from 'mobx';
import {disposeOnUnmount} from 'mobx-react';
import * as css from './Dashboard.css'
import { autorun, React, inject, observer } from '@api';
//import {SiteHeader, TopBar, RightSidebar, DashboardStore, GroupByCatalog, ProductDetailsModal, SidebarDrawerContainer} from '@design-board/components'


interface MyProps {
  gridRef?: (grid) => void,
  customAttributeDefinitions?: any
}

@observer
export class DashboardComponent extends React.Component <MyProps> {

  render() {
    const {props: {gridRef}} = this
    return (
      <div className={css.root}>
        {/*<SiteHeader className={css.siteHeader} selection={selection} showUser showPresentation={false} showExport*/}
        {/*            showSearchIcon showBackButton={false}/>*/}
        <div className={css.grid} ref={gridRef}>
          <div className={css.rightPanel}>
            <div className={css.topBar}>
              {/*<TopBar/>*/}
              TopBar
            </div>
            {/*<GroupByCatalog />*/}
            GroupByCatalog
          </div>
        </div>
        {/*{false && !site.touchDevice ? <ProductDetailsModal/> : <RightSidebar/>}*/}
        {/*<SidebarDrawerContainer/>*/}
      </div>
    )
  }
}