import {React} from '@api';
import {DashboardComponent} from './Dashboard'

export const DashboardLayout = ({children}) => {

  return <div>
    Dashboard

    <DashboardComponent/>
    {children}
  </div>
}

