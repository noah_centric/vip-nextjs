import {withApollo} from '../stores';
import * as React from 'react'
import Link from 'next/link'
import Layout from '../components/Layout'
import { NextPage } from 'next'

const IndexPage: NextPage = () => {
  return (
    <Layout title="Home | Next.js + TypeScript Example">
      <h1>VIP Next-Gen 👋</h1>
      <p>
        <Link href="/concept-board">
          <a>Concept Board</a>
        </Link>
      </p>
      <p>
        <Link href="/buying-board">
          <a>Buying Board</a>
        </Link>
      </p>
    </Layout>
  )
}

export default withApollo(IndexPage)
