import {withApollo} from '@stores';
import {React} from '@api'
import Link from 'next/link'
import {Layout} from '@components'
import {NextPage} from 'next'
import {DashboardLayout} from '@modules/concept-board'
const IndexPage: NextPage = () => {
  return (
    <DashboardLayout >
      <h1>Concept Board</h1>
    </DashboardLayout>
  )
}

export default withApollo(IndexPage)
