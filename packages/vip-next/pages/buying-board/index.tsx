import {withApollo} from '@stores';
import {React} from '@api'
import Link from 'next/link'
import {Layout} from '@components'
import {NextPage} from 'next'

const IndexPage: NextPage = () => {
  return (
    <Layout title="Home | Next.js + TypeScript Example">
      <h1>Buying Board</h1>
    </Layout>
  )
}

export default withApollo(IndexPage)
