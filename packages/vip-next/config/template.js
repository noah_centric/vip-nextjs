const config = {
  apiGatewayEndpoint: '{{DESIGN_BOARD_API_GATEWAY_ENDPOINT}}',
  graphqlEndpoint: '{{DESIGN_BOARD_GRAPHQL_ENDPOINT}}',
  wsApiGatewayEndpoint: '{{DESIGN_BOARD_API_GATEWAY_WS_ENDPOINT}}',
  mediaMicroserviceEndpoint: '{{MEDIA_MICROSERVICE_ENDPOINT}}',
  centricURL: '{{CENTRIC_URL}}',
  designBoardAppLogo: '{{DESIGN_BOARD_APP_LOGO}}',
  vipConfiguratorFeature: `{{VIP_CONFIGURATOR_FEATURE}}`,
  vipConfiguratorDeleteOptions: `{{VIP_CONFIGURATOR_DELETE_OPTIONS}}`,
  publishToPLM: '{{PUBLISH_TO_PLM}}',
  showStorybook: '{{SHOW_STORYBOOK}}',
  leToken: '{{LE_TOKEN}}',
  enableResponseTimeLogging: '{{ENABLE_RESPONSE_TIME_LOGGING}}',
  grantDayPassFeature: '{{GRANT_DAY_PASS_FEATURE}}',
  boards: '{{BOARDS}}'
}

export default config
