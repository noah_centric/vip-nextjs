const config = {
  apiGatewayEndpoint: 'http://vlp-board-dev.centricsoftware.com/design-board-api-gateway/',
  graphqlEndpoint: 'http://vlp-board-dev.centricsoftware.com/design-board-graphql/',
  wsApiGatewayEndpoint: '',
  mediaMicroserviceEndpoint: 'http://vlp-board-dev.centricsoftware.com/centric-media-microservice/',
  centricURL: 'https://nextgen.centricsoftware.com/WebAccess/home.html',
  designBoardAppLogo: 'CENTRIC',
  vipConfiguratorFeature: `on`,
  vipConfiguratorDeleteOptions: `off`,
  publishToPLM: 'true',
  showStorybook: 'true',
  leToken: '',
  enableResponseTimeLogging: 'false',
  grantDayPassFeature: 'on',
  boards: 'designBoard,merchBoard'
}

export default config
