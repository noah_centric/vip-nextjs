const config = {
  apiGatewayEndpoint: 'http://design-board2-dev-legacy.centricsoftware.com/design-board-api-gateway/',
  graphqlEndpoint: 'http://design-board2-dev-legacy.centricsoftware.com/design-board-graphql/',
  wsApiGatewayEndpoint: 'ws://design-board2-dev-legacy.centricsoftware.com/design-board-graphql/',
  mediaMicroserviceEndpoint: 'http://concept-board-dev.centricsoftware.com/centric-media-microservice/',
  centricURL: 'http://nextgen.centricsoftware.com/WebAccess/home.html',
  designBoardAppLogo: 'CENTRIC',
  vipConfiguratorFeature: `on`,
  vipConfiguratorDeleteOptions: `off`,
  publishToPLM: 'true',
  showStorybook: 'true',
  leToken: '',
  enableResponseTimeLogging: 'false',
  grantDayPassFeature: 'on'
}

export default config
