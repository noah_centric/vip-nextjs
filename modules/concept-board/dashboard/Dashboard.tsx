import {nav, sidebar, site, stores} from '@stores';
import {observable} from 'mobx';
import {disposeOnUnmount} from 'mobx-react';
import * as css from './Dashboard.css'
import { autorun, React, inject, observer, RouteComponentProps } from '@api';
import {SiteHeader, TopBar, RightSidebar, DashboardStore, GroupByCatalog, ProductDetailsModal, SidebarDrawerContainer} from '@design-board/components'
import { apolloCache} from '@api/graphql'

export type DashboardRouteParams = { productId, season, category1, category2, }

export interface DashboardRouteProps extends Partial<RouteComponentProps<DashboardRouteParams>> {

}

interface MyProps {
  selection?: apolloCache.SelectedSeason,
  gridRef?: (grid) => void,
  customAttributeDefinitions?: any
  dashboard?: DashboardStore
}

@inject('dashboard')
@observer
export class DashboardComponent extends React.Component <MyProps> {
  @observable sidebarEverOpen = false
  @disposeOnUnmount sidebarOpened = autorun(() => this.sidebarEverOpen = this.sidebarEverOpen || sidebar.open)
  render() {
    const {props: {gridRef, selection, dashboard: {groupBy}}} = this
    return (
      <div className={css.root}>
        <SiteHeader className={css.siteHeader} selection={selection} showUser showPresentation={false} showExport
                    showSearchIcon showBackButton={false}/>
        <div className={css.grid} ref={gridRef}>
          <div className={css.rightPanel}>
            <div className={css.topBar}>
              <TopBar/>
            </div>
            <GroupByCatalog />
          </div>
        </div>
        {false && !site.touchDevice ? <ProductDetailsModal/> : <RightSidebar/>}
        <SidebarDrawerContainer/>

      </div>
    )
  }
}
